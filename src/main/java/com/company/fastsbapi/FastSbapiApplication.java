package com.company.fastsbapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.company.fastsbapi.mapper")
public class FastSbapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastSbapiApplication.class, args);
	}
}
