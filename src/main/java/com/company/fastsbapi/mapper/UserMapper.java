package com.company.fastsbapi.mapper;

import com.company.fastsbapi.dataobject.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
public interface UserMapper {
    User getUserName();
}
